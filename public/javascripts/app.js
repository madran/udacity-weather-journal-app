const key = '7db0c18e848b6efe75af245d5ee9811a';

const generate = document.getElementById('generate')
const form = document.getElementById('form');

form.addEventListener('submit', e => {

    e.preventDefault();
});

generate.addEventListener('click', async e => {

    e.preventDefault();

    let zipElement = document.getElementById('zip')
    let zip = zipElement.value;

    if (zip === '') {
        displayZipError(zipElement);
        return;
    } else {
        hideZipError(zipElement);
    }

    let userData = {
        temperature: await loadWeatherTemperature(zip),
        date: new Date().toDateString(),
        userInput: document.getElementById('feelings').value
    }

    await sendUserData('/weather', userData);
    userData = await fetch('/weather');
    userData = await userData.json();
    showUserData(userData);
});

async function loadWeatherTemperature(zip) {

    if (zip && zip.trim().length) {
        const result = await fetch(`http://api.openweathermap.org/data/2.5/weather?zip=${zip},us&appid=${key}&units=metric`)
        let weather = await result.json();
        return weather.main.temp;
    }
}

async function sendUserData(url, userData) {

    await fetch(url,{
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(userData)
    });
}

function showUserData(data) {

    let table = document.getElementById('table');

    let temperatureTd = document.createElement('td')
    temperatureTd.innerHTML = data.temperature;

    let dateTd = document.createElement('td')
    dateTd.innerHTML = data.date;

    let userInputTd = document.createElement('td')
    userInputTd.innerHTML = data.userInput;

    let tr = document.createElement('tr')
    tr.appendChild(temperatureTd);
    tr.appendChild(dateTd);
    tr.appendChild(userInputTd);

    table.appendChild(tr);
}

function displayZipError(zipElement) {

    zipElement.classList.add('zip--error');
    zipElement.parentElement.classList.add('zip-container--error')
}

function hideZipError(zipElement) {

    zipElement.classList.remove('zip--error');
    zipElement.parentElement.classList.remove('zip-container--error')
}