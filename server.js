const port = 8000;

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());

app.use('/', require('./routes/api'))

app.listen(
    port,
    () => {
        console.log('Server starting on port: ' + port);
    }
);