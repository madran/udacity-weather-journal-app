const express = require('express');
const projectData = {};

const router = express.Router();

router.get('/weather', (req, res) => {

    res.json(projectData);
});

router.post('/weather', (req, res) => {

    projectData.temperature = req.body.temperature;
    projectData.date = req.body.date;
    projectData.userInput = req.body.userInput;

    res.status(200).send();
});

module.exports = router;
